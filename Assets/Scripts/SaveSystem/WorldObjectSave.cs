﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class WorldObjectSave : Save {
	public Data data;
	private WorldObject worldObject;
	private string jsonString;

	//Class containing info a worldObject needs to save
	[Serializable]
	public class Data : BaseData {
		public Vector3 position;
		public Vector3 eulerAngles;
		public ObjectState state;
		public string ParentName;
	}

	void Awake() {
		worldObject = GetComponent<WorldObject>();
		data = new Data();
	}

	public override string Serialize() {
		data.prefabName = prefabName;
		data.position = worldObject.transform.position;
		data.eulerAngles = worldObject.transform.eulerAngles;
		data.state = worldObject.objectState;
		data.ParentName = transform.parent.name;
		jsonString = JsonUtility.ToJson(data);
		return (jsonString);
	}

	public override void Deserialize(string jsonData) {
		JsonUtility.FromJsonOverwrite(jsonData, data);

		worldObject.transform.position = data.position;
		worldObject.transform.eulerAngles = data.eulerAngles;
		worldObject.objectState = data.state;
		worldObject.parentName = data.ParentName;
		worldObject.name = prefabName;
	}
}
