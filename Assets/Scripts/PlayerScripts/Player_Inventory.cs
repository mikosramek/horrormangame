﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Inventory : MonoBehaviour {
	public List<Item> myItems;

	void Start() {
	}

	public void Give(Item item) {
		if (!myItems.Contains(item)) {
			myItems.Add(item);
		}
	}

	public void Take(Item item) {
		if (myItems.Contains(item)) {
			myItems.Remove(item);
		}
	}
}
