﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventSystem_Listener : MonoBehaviour {
	public Flag ToReceive;

	//GenericInteraction gInt;
	public Vector3 movement;
	public Vector3 rotation;
	public bool relative;
	public UnityEvent GenericEvents;

	public void Awake() {
		//gInt = GetComponent<GenericInteraction>();
	}

	public void OnEnable() {
		EventManager.StartListening(ToReceive.message, Action);
	}

	public void OnDisable() {
		EventManager.StopListening(ToReceive.message, Action);
	}

	void Action() {
		/*
		if (gInt == null) {
			Debug.LogWarning(gameObject.name + " doesn't have a GenericInteraction componenet on it. This listener will do nothing.");
			return;
		}
		*/
		Debug.Log(gameObject.name + " received " + ToReceive.message);
		if (relative) {
			transform.localPosition += movement;
			transform.eulerAngles += rotation;
		}
		else {
			transform.eulerAngles = rotation;
			transform.localPosition = movement;
		}
		GenericEvents.Invoke();
		//gInt.Interact();
	}

	void Action(string eventName) {
	}
}
