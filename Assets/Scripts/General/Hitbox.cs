﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Hitbox {

	[SerializeField]
	public Rect rect;

	public Hitbox(Vector2 size) {
		//rect = new Rect(Vector2.zero, size);
	}

	public Rect GetHitbox() {
		return rect;
	}

	public Rect ShiftHitbox(Vector3 pos) {
		return new Rect(pos - new Vector3(rect.size.x / 2 + rect.x, rect.size.y / 2 + rect.y), rect.size);
	}
}
