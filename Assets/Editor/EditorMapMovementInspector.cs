﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EditorMapMovement))]
public class EditorMapMovementInspector : Editor {

	public override void OnInspectorGUI() {
		EditorMapMovement myTarget = (EditorMapMovement)target;
		GUILayout.Label(myTarget.currentCamRoom.ToString());
		//myTarget.mapC = (MapController)EditorGUILayout.ObjectField(myTarget.mapC, typeof(MapController), true);
		if (myTarget.rooms != null) {
			if (GUILayout.Button("Update Rooms")) {
				myTarget.FindRooms();
			}
			for (int i = 0; i < myTarget.rooms.Length; i++) {
				if (GUILayout.Button(myTarget.rooms[i].RoomName)) {
					myTarget.MoveToRoom(myTarget.rooms[i]);
				}
			}
		}
		else {
			if (GUILayout.Button("Find Rooms")) {
				myTarget.FindRooms();
			}
		}
	}
}
