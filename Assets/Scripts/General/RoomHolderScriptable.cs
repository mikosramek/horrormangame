﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomHolderScriptable : ScriptableObject {
	public Act[] acts;
}

[System.Serializable]
public class Act {

	[SerializeField]
	public Room[] rooms;
}
