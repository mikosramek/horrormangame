﻿[System.Flags]
public enum Tags {
	nonmoving = 0,
	interactable = 1 << 0,
	inventory = 1 << 1,
	important = 1 << 2
}
