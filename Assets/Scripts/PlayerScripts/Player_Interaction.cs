﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Interaction : MonoBehaviour {

	[Header("Player Stats")]
	public Hitbox hitbox;

	//public Vector2 hitboxSize;
	public Tags myTags;
	public SpriteRenderer cursorSprite;
	MapController mapController;
	Room currentRoom;
	WorldObject objectCurrentlyOver;
	bool canInteract;

	[Header("Internal Player Refs")]
	public ShortDescription shortDescription;
	public DialogueBox dialogueBox;
	Player_Input myInput;

	//Some sort of currentItem?
	public GameObject[] cursorObjects;

	void Awake() {
		myInput = GetComponent<Player_Input>();
		mapController = FindObjectOfType<MapController>();
	}

	void Start() {
		//hitbox = new Hitbox(hitboxSize);
		UpdateRoom();
	}

	void Update() {
		//If you're over an object
		if (canInteract) {
			//and that object exists
			if (objectCurrentlyOver != null) {
				//if you Inspect
				if (myInput.GetInspect()) {
					//If it can be used
					if (objectCurrentlyOver.objectState == ObjectState.interactable) {
						shortDescription.UpdateDescription(objectCurrentlyOver.objectInspectDescription);
					}
					//Can add Special Here
					else if (objectCurrentlyOver.objectState == ObjectState.special) {
						//nothing for now
					}
					else if (objectCurrentlyOver.objectState == ObjectState.hidden) {
						//nothing
					}
					//It has been used
					else {
						string text = objectCurrentlyOver.objectUsedDescription;
						if (text.Equals("")) {
							shortDescription.UpdateDescription(NonDescriptionBank.GetANoNewInfoPhrase());
						}
						else {
							shortDescription.UpdateDescription(objectCurrentlyOver.objectUsedDescription);
						}
					}
				}
				//If you Interact
				else if (myInput.GetInteract()) {
					//If it's interactable
					if (objectCurrentlyOver.objectState == ObjectState.interactable) {
						string text = objectCurrentlyOver.objectInteractDescription;
						//If it doesn't have an interaction text - meaning it's just viewable - grab a nothing phrase
						if (text.Equals("")) {
							shortDescription.UpdateDescription(NonDescriptionBank.GetANothingPhrase());
						}
						//Otherwise use the text it has
						else {
							shortDescription.UpdateDescription(text);
							objectCurrentlyOver.Interact();
						}
					}
					else if (objectCurrentlyOver.objectState == ObjectState.special) {
						//nothing for now
					}
					else if (objectCurrentlyOver.objectState == ObjectState.hidden) {
						//nothing
					}
					//If it's used
					else {
						shortDescription.UpdateDescription(NonDescriptionBank.GetANothingPhrase());
					}
				}
				//If you talk
				else if (myInput.GetTalk()) {
					if (objectCurrentlyOver.objectState == ObjectState.interactable) {
						if (objectCurrentlyOver.dialogue != null) {
							DialogueHolder dialogueTree = objectCurrentlyOver.dialogue;
							foreach (TalkingPoint t in dialogueTree.talkingPoints) {
								if (t.startingPoint) {
									dialogueBox.StartDialogue();
									dialogueBox.UpdateDialogueBox(t);
									break;
								}
							}
						}
						else {
							shortDescription.UpdateDescription(NonDescriptionBank.GetANonDialogue());
						}
					}
				}
			}
		}
	}

	public void DialogueCallback(int link) {
		if (link == -1) {
			dialogueBox.EndDialogue();
			return;
		}
		dialogueBox.UpdateDialogueBox(objectCurrentlyOver.dialogue.FindViaID(link));
	}

	void FixedUpdate() {
		Rect currentHitboxRect = hitbox.ShiftHitbox(transform.position);
		foreach (WorldObject obj in currentRoom.GetRoomObjects()) {
			if (obj == null) {
				currentRoom.UpdateRoomObjects();
				return;
			}
			Rect objectHitbox = obj.hitbox.ShiftHitbox(obj.transform.position);//new Rect(obj.transform.position, obj.hitbox.rect.size);
			if (currentHitboxRect.Overlaps(objectHitbox) && obj.objectState != ObjectState.hidden) {// && obj.CanInteract()) {
				SetCanInteract(true, obj);
				break;
			}
			SetCanInteract(false, null);
		}
	}

	public void SetCanInteract(bool set, WorldObject obj) {
		objectCurrentlyOver = obj;
		canInteract = set;
		cursorObjects[1].SetActive(set);
		cursorObjects[0].SetActive(!set);
		//cursorSprite.sprite = set ? cursorObjects[1].SetActive(true) : cursorObjects[0].SetActive(true);
	}

	public void UpdateRoom() {
		currentRoom = mapController.GetCurrentRoom();
		currentRoom.UpdateRoomObjects();
	}

	public void UpdateRoom(Room newRoom) {
		currentRoom = newRoom;
		currentRoom.UpdateRoomObjects();
	}

	void OnDrawGizmos() {
		if (GlobalSettings.ShowDebug) {
			if (hitbox != null) {
				Rect current = hitbox.ShiftHitbox(transform.position);
				Gizmos.DrawWireCube(current.center, current.size);
			}
		}
	}
}
