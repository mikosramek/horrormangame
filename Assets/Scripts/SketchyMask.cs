﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SketchyMask : MonoBehaviour {
	public SpriteRenderer spo;
	public SpriteMask spm;

	void Update() {
		spm.sprite = spo.sprite;
	}
}
