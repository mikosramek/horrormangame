﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ActEditor : Editor {

	[MenuItem("Assets/Create/Room Holder")]
	public static void CreateMyAsset() {
		RoomHolderScriptable asset = ScriptableObject.CreateInstance<RoomHolderScriptable>();

		AssetDatabase.CreateAsset(asset, "Assets/NewRoomHolder.asset");
		AssetDatabase.SaveAssets();
	}
}
