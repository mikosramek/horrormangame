﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Input : MonoBehaviour {
	bool p_up, p_down, p_left, p_right;
	bool[] directions;
	bool map, inventory, interact, inspect, talk;

	void Awake() {
		directions = new bool[4];
	}

	void Start() {
	}

	void Update() {
		float vert = Input.GetAxisRaw("Vertical");
		float horz = Input.GetAxisRaw("Horizontal");
		directions[0] = vert > 0;
		directions[2] = vert < 0;
		directions[1] = horz > 0;
		directions[3] = horz < 0;

		map = Input.GetButtonDown("map");
		inventory = Input.GetButtonDown("inventory");
		interact = Input.GetButtonDown("interact");
		inspect = Input.GetButtonDown("inspect");
		talk = Input.GetButtonDown("talk");
	}

	public bool[] GetDirections() {
		return directions;
	}

	public bool GetMap() {
		return map;
	}

	public bool GetInventory() {
		return inventory;
	}

	public bool GetInspect() {
		return inspect;
	}

	public bool GetInteract() {
		return interact;
	}

	public bool GetTalk() {
		return talk;
	}
}
