﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueResponseButton : MonoBehaviour {
	Player_Interaction pi;
	int link;

	public void Create(string r, int l) {
		pi = FindObjectOfType<Player_Interaction>();
		GetComponentInChildren<Text>().text = r;
		link = l;
	}

	public void Clicked() {
		pi.DialogueCallback(link);
	}
}
