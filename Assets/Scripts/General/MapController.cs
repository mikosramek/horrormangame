﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour {
	Room[] rooms;
	Camera cam;
	public Player_Cursor pc;
	public Player_Interaction pi;

	//TO SAVE
	int currentRoom = 0;
	int currentAct = 0;
	int startingRoom = 0;

	void Awake() {
		rooms = FindObjectsOfType<Room>();
		Array.Sort(rooms, delegate (Room x, Room y) { return x.RoomNumber.CompareTo(y.RoomNumber); });
	}

	public Room GetCurrentRoom() {
		return rooms[currentRoom];
	}

	public void MoveToRoom(Room room) {
		currentRoom = room.RoomNumber;
		pc.transform.position = rooms[currentRoom].transform.position;
		pi.UpdateRoom(rooms[currentRoom]);
		MoveCamera();
	}

	void MoveCamera() {
		if (cam == null) {
			cam = FindObjectOfType<Camera>();
		}
		Vector3 target = rooms[currentRoom].transform.position;
		cam.transform.position = new Vector3(target.x, target.y, cam.transform.position.z);
	}
}
