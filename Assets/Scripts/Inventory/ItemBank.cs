﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBank : MonoBehaviour {
	public List<Item> items;
	public List<ItemEvent> events;
	public Player_Inventory player_Inventory;

	void Start() {
	}

	public void OnEnable() {
		foreach (ItemEvent ie in events) {
			EventManager.StartListening(ie.TriggerMessage, ItemEventHandler);
		}
	}

	public void OnDisable() {
		foreach (ItemEvent ie in events) {
			EventManager.StopListening(ie.TriggerMessage, ItemEventHandler);
		}
	}

	public void ItemEventHandler(string eventName) {
		foreach (ItemEvent ie in events) {
			if (ie.TriggerMessage.Equals(eventName)) {
				if (ie.itemToGive) {
					player_Inventory.Give(ie.itemToGive);
				}
				if (ie.itemToTake) {
					player_Inventory.Take(ie.itemToTake);
				}
			}
		}
	}

	public void ItemEventHandler() {
	}
}

public class ItemEvent {
	public string TriggerMessage;
	public Item itemToGive;
	public Item itemToTake;
}
