﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class VoidMesh : MonoBehaviour {
	public int xSize, ySize;
	private Vector3[] vertices;
	private Mesh mesh;

	private void Awake() {
		Generate();
	}

	void Update() {
		//MoveMesh();
	}

	void MoveMesh() {
		for (int y = 0; y < vertices.Length; y++) {
			Vector3 vectorToMiddle = transform.position - vertices[y];
			vertices[y] = vertices[y] * Mathf.Sin(vertices[y].x);
		}
		mesh.vertices = vertices;
	}

	private void Generate() {
		GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Procedural Grid";

		/*vertices = new Vector3[(xSize + 1) * (ySize + 1)];
		Vector2[] uv = new Vector2[vertices.Length];
		for (int i = 0, y = 0; y <= ySize; y++) {
			for (int x = 0; x <= xSize; x++, i++) {
				vertices[i] = new Vector3(Mathf.Sin(x), Mathf.Cos(y));
				uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
			}
		}*/
		vertices = new Vector3[(xSize + 1) * (ySize + 1)];
		Vector2[] uv = new Vector2[vertices.Length];
		for (int y = 0; y < vertices.Length; y++) {
			float a = Mathf.Cos(y);
			float b = Mathf.Sin(y);
			vertices[y] = new Vector3(a, b);
			uv[y] = new Vector2(a / xSize, b / ySize);
		}
		mesh.vertices = vertices;
		mesh.uv = uv;
		int[] triangles = new int[xSize * ySize * 6];
		for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++) {
			for (int x = 0; x < xSize; x++, ti += 6, vi++) {
				triangles[ti] = vi;
				triangles[ti + 3] = triangles[ti + 2] = vi + 1;
				triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
				triangles[ti + 5] = vi + xSize + 2;
			}
		}
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
	}

	private void OnDrawGizmos() {
		Gizmos.color = Color.black;
		if (vertices == null) {
			return;
		}
		for (int i = 0; i < vertices.Length; i++) {
			Gizmos.DrawSphere(vertices[i] + transform.position, 0.1f);
		}
	}
}
