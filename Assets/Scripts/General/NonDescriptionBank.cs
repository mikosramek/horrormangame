﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonDescriptionBank : MonoBehaviour {

	//@TODO Add more phrases
	static string[] NothingPhrases = {
		"Nothing happens...",
		"You can't see any changes...",
		"Your attempts are fruitless..."
	};

	static string[] NoNewInfoPhrases = {
		"Nothing else to see here..."
	};

	static string[] NonDialogues = {
		"There's no response to your words...",
		"Your words fall on deaf ears, or no ears at all..."
	};

	public static string GetANoNewInfoPhrase() {
		if (NoNewInfoPhrases.Length < 0) {
			Debug.LogError("You need some No New Info phrases!");
			return "";
		}
		return NoNewInfoPhrases[Random.Range(0, NoNewInfoPhrases.Length)];
	}

	public static string GetANothingPhrase() {
		if (NothingPhrases.Length < 0) {
			Debug.LogError("You need some nothing phrases!");
			return "";
		}
		return NothingPhrases[Random.Range(0, NothingPhrases.Length)];
	}

	public static string GetANonDialogue() {
		if (NonDialogues.Length < 0) {
			Debug.LogError("You need some Non Dialogues!");
			return "";
		}
		return NonDialogues[Random.Range(0, NonDialogues.Length)];
	}
}
