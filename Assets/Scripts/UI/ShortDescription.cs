﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShortDescription : MonoBehaviour {
	public float letterSpeed;
	public float delayForFade;

	//public Text descriptionText;
	public TextMeshProUGUI dText;
	public RectTransform bg;

	public void UpdateDescription(string words) {
		dText.text = "";
		bg.sizeDelta = new Vector2(0, bg.rect.height);
		StopAllCoroutines();
		StartCoroutine(UpdateString(words));
	}

	IEnumerator UpdateString(string words) {
		int letterCount = 0;
		//float targetLength = 0.23f * words.Length;
		//float step = 1 / (float)words.Length;
		//float i = 0;
		float totalWidth = 0.075f;
		while (letterCount != words.Length) {
			//CharacterInfo cInfo;

			//descriptionText.text += words[letterCount];
			dText.text += words[letterCount];
			//descriptionText.font.GetCharacterInfo(descriptionText.text[letterCount], out cInfo, 0);

			//float width = Mathf.Lerp(0, targetLength, i);
			totalWidth += 0.23f;//cInfo.advance;
								//print(totalWidth + " + " + cInfo.advance);
								//bg.sizeDelta = new Vector2(totalWidth, bg.rect.height);

			letterCount++;
			//i += step;
			yield return new WaitForSeconds(letterSpeed);
		}
		yield return new WaitForSeconds(delayForFade);
		dText.text = "";
		bg.sizeDelta = new Vector2(0, bg.rect.height);
		yield return null;
	}
}
