﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugOptions : MonoBehaviour {
	public KeyCode showDebug;

	void Update() {
		if (Input.GetKeyDown(showDebug)) {
			GlobalSettings.ShowDebug = !GlobalSettings.ShowDebug;
			//Debug.Log("Show Debug set to: " + GlobalSettings.ShowDebug);
		}
	}
}
