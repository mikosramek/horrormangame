﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {
	public string RoomName;
	public int RoomNumber;

	public Vector3 GizmoGuideSize = new Vector3(17.25f, 10);

	//Act number?

	WorldObject[] objectsInRoom;

	void Awake() {
		objectsInRoom = GetComponentsInChildren<WorldObject>();
	}

	public void UpdateRoomObjects() {
		objectsInRoom = GetComponentsInChildren<WorldObject>();
	}

	public WorldObject[] GetRoomObjects() {
		return objectsInRoom;
	}

	void OnDrawGizmos() {
		Gizmos.DrawWireCube(transform.position, GizmoGuideSize);
	}
}
