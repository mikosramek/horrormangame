﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSystem_GlobalFlags : ScriptableObject {
	public List<Flag> flags;

	public void EditorSave() {
#if UNITY_EDITOR
		UnityEditor.EditorUtility.SetDirty(this);
#endif
	}

	public void Save() {
	}

	public void Load() {
	}
}
