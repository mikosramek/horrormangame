﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSaveManager : MonoBehaviour {
	public string gameDataFilename = "game-save.json";
	private string gameDataFilePath;
	private List<string> saveItems;
	private bool firstPlay = true;

	public static GameSaveManager Instance { get; private set; }

	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	void Awake() {
		// First we check if there are any other instances conflicting
		if (Instance != null && Instance != this) {
			// If that is the case, we destroy other instances
			Destroy(gameObject);
			return;
		}

		// Here we save our singleton instance
		Instance = this;

		// Furthermore we make sure that we don't destroy between scenes (this is optional)
		DontDestroyOnLoad(gameObject);

		gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
		saveItems = new List<string>();
	}

	public void AddObject(string item) {
		saveItems.Add(item);
	}

	public void Save() {
		//Clear the list to make sure nothing is getting saved twice
		saveItems.Clear();

		//Find all the scripts that inherit from Save, ie TankSave, TurretSave
		Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
		//Add them to the list of objects to save
		foreach (Save saveableObject in saveableObjects) {
			saveItems.Add(saveableObject.Serialize());
		}
		print(gameDataFilePath);
		//StreamWriter allows the user to write to a text file, in this case game-save.json
		//It then writes all the items to be saved to a line/item
		using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
			foreach (string item in saveItems) {
				gameDataFileStream.WriteLine(item);
			}
		}
	}

	public void Load() {
		//Used to flag if there is save data versus no save data
		firstPlay = false;
		//Clear the saveItems list, so in this case, nothing is then loaded twice
		saveItems.Clear();
		//Tells Unity to Load the scene
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	//Runs whenever the scene is loaded
	private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		//If it's the first time running the scene, just set it up like the scene within the Unity Scene View
		if (firstPlay)
			return;

		//Get the save data from the json file
		LoadSaveGameData();
		//Destory All the items
		DestroyAllSaveableObjectsInScene();
		//Reinstantiate them in the state they were saved in
		CreateGameObjects();
		//Call Player_Load event
		FindObjectOfType<Player_Load>().OnLoad.Invoke();
	}

	void LoadSaveGameData() {
		//Using StreamReader to read in from a text file, or in this case game-save.json
		using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
			//Peek() - returns an integer representing the next character to be read
			//         returns -1 if there is nothing to be read / can't be read
			while (gameDataFileStream.Peek() >= 0) {
				//Read the next line if it exists, and then get rid of whitespace
				string line = gameDataFileStream.ReadLine().Trim();
				//If the string line actually exists, ie longer than 0, through it into the list of saveItems
				if (line.Length > 0) {
					saveItems.Add(line);
				}
			}
		}
	}

	void DestroyAllSaveableObjectsInScene() {
		//Find all objects with scripts inheriting from Save and then destroy them from the scene
		Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
		foreach (Save saveableObject in saveableObjects) {
			Destroy(saveableObject.gameObject);
		}
	}

	void CreateGameObjects() {
		//Go through every item (line) that was added into saveItems
		//Based on the foreach, this is able to match the item name / proper prefab / Json data for each item
		//For example, if the line had the TankPrefab as the prefabName, it would be able to instatiate the proper prefab, then send this particular line to the tank to use
		foreach (string saveItem in saveItems) {
			//Find the indexes of where the prefab name starts and ends
			string pattern = @"""prefabName"":""";
			int patternIndex = saveItem.IndexOf(pattern);
			int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
			int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
			//Get the string within the line, the substring, that starts at "", and ends at "", with these indexes being found out above
			string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

			//Instatiate the saved item, using the string within the json to determine the proper prefab
			GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
			//Send the line pertaining to this saveItem to the saveItem, basically sending the Save data to it
			//for it to deserialize and use
			item.SendMessage("Deserialize", saveItem);
		}
	}
}
