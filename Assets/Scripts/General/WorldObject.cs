﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObject : MonoBehaviour {
	public Tags objectType;
	public string objectName;
	public string objectInspectDescription;
	public string objectUsedDescription;
	public string objectInteractDescription;
	public string objectInventoryDescription;
	public string objectUniqueInteractionText;
	public DialogueHolder dialogue;
	public Hitbox hitbox;
	//public Vector2 hitboxSize;

	//For saving
	public ObjectState objectState;

	[HideInInspector]
	public string parentName;

	//Interaction
	GenericInteraction gInt;
	SpriteRenderer[] spriteArray;

	void Awake() {
		//hitbox = new Hitbox(hitboxSize);
		gInt = GetComponent<GenericInteraction>();
		spriteArray = GetComponentsInChildren<SpriteRenderer>();
	}

	void Start() {
		if (!parentName.Equals("") && transform.parent == null) {
			transform.SetParent(GameObject.Find(parentName).transform);
		}
		if (objectState == ObjectState.hidden) {
			Hide();
		}
	}

	public void ChangeState(ObjectState newState) {
		objectState = newState;
	}

	public void UseUp() {
		objectState = ObjectState.used;
	}

	public void EnableInteraction() {
		objectState = ObjectState.interactable;
	}

	public void Hide() {
		for (int i = 0; i < spriteArray.Length; i++) {
			spriteArray[i].enabled = false;
		}
		objectState = ObjectState.hidden;
	}

	public void UnHide() {
		for (int i = 0; i < spriteArray.Length; i++) {
			spriteArray[i].enabled = true;
		}
	}

	public void Lock() {
		objectState = ObjectState.locked;
	}

	//@TODO add generic flag ability
	public void Interact() {
		print(gameObject.name + " is interacted with.");
		if (gInt != null) {
			gInt.Interact();
		}
	}

	public bool CanInteract() {
		if (objectType != Tags.interactable) {
			return false;
		}
		if (objectState == ObjectState.interactable) {
			return true;
		}
		return false;
	}

	void OnDrawGizmos() {
		if (GlobalSettings.ShowDebug) {
			if (hitbox != null) {
				Rect current = hitbox.ShiftHitbox(transform.position);
				Gizmos.DrawWireCube(current.center, current.size);
			}
			Color gCol = Color.white;
			switch (objectState) {
				case ObjectState.hidden:
					gCol = Color.cyan;
					break;

				case ObjectState.interactable:
					gCol = Color.green;
					break;

				case ObjectState.locked:
					gCol = Color.red;
					break;

				case ObjectState.special:
					gCol = Color.yellow;
					break;

				case ObjectState.used:
					gCol = Color.grey;
					break;
			}
			Gizmos.color = gCol;
			Gizmos.DrawSphere(transform.position, 0.1f);
		}
	}
}

public enum ObjectState {
	locked,
	interactable,
	used,
	special,
	hidden
}
