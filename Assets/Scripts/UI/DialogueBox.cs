﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour {
	public GameObject holder;
	public Text characterText, characterName;
	public Image characterPortrait;
	public GameObject ResponseBox, ResponseHolder;

	public void Start() {
		holder.SetActive(false);
	}

	public void StartDialogue() {
		holder.SetActive(true);
	}

	public void UpdateDialogueBox(TalkingPoint t) {
		ClearResponses();
		characterName.text = t.npc.characterName;
		characterPortrait.sprite = t.npc.portraits[t.npc.currentPortrait].image;
		characterText.text = t.text;
		DialogueResponseButton drbTemp;
		foreach (TalkingResponse tr in t.responses) {
			drbTemp = Instantiate(ResponseBox, ResponseHolder.transform).GetComponent<DialogueResponseButton>();
			drbTemp.Create(tr.text, tr.responseID);
		}
		drbTemp = Instantiate(ResponseBox, ResponseHolder.transform).GetComponent<DialogueResponseButton>();
		drbTemp.Create("[Leave]", -1);
	}

	public void EndDialogue() {
		holder.SetActive(false);
	}

	void ClearResponses() {
		Transform[] children = ResponseHolder.GetComponentsInChildren<Transform>();
		for (int i = 0; i < children.Length; i++) {
			if (children[i] != ResponseHolder.transform) {
				Destroy(children[i].gameObject);
			}
		}
	}
}
