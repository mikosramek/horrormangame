﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenericInteraction : MonoBehaviour {
	public Vector3 RelativeMovement;
	public Vector3 RelativeRotation;
	public UnityEvent genericEvent;
	public Flag genericMessage;

	public void Interact() {
		transform.localPosition += RelativeMovement;
		transform.eulerAngles += RelativeRotation;
		genericEvent.Invoke();
		if (!genericMessage.message.Equals("")) {
			EventManager.TriggerEvent(genericMessage.message, this.gameObject);
			Debug.Log(gameObject.name + " fired off " + genericMessage.message);
		}
	}
}
