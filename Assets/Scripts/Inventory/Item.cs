﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject {
	public string itemName;
	public Sprite InventorySprite;
	public bool isUsable;
	public string messageOnUse;
}
