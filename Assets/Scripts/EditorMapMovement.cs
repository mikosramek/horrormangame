﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EditorMapMovement : MonoBehaviour {
	MapController mapC;
	public Room[] rooms;
	Camera cam;
	public int currentCamRoom;

	void Awake() {
		mapC = GetComponent<MapController>();
#if UNITY_EDITOR
		mapC.MoveToRoom(rooms[currentCamRoom]);
#endif
	}

	public void FindRooms() {
		rooms = FindObjectsOfType<Room>();
		Array.Sort(rooms, delegate (Room x, Room y) { return x.RoomNumber.CompareTo(y.RoomNumber); });
	}

	public void MoveToRoom(Room room) {
		//print("Moving to the " + rooms[roomNumber].RoomName);
		if (cam == null) {
			cam = FindObjectOfType<Camera>();
		}
		currentCamRoom = room.RoomNumber;
		Vector3 target = rooms[room.RoomNumber].transform.position;
		cam.transform.position = new Vector3(target.x, target.y, cam.transform.position.z);
	}
}
