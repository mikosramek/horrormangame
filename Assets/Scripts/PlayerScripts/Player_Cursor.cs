﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Cursor : MonoBehaviour {
	Player_Input myInput;
	public float cursorSpeed;

	void Awake() {
		myInput = GetComponent<Player_Input>();
		if (myInput == null) {
			Debug.LogError(gameObject.name + " needs an Input script!");
		}
	}

	void Update() {
		bool[] input = myInput.GetDirections();
		Vector3 movement = Vector3.zero;
		if (input[0]) {
			movement += new Vector3(0, 1);
		}
		if (input[1]) {
			movement += new Vector3(1, 0);
		}
		if (input[2]) {
			movement -= new Vector3(0, 1);
		}
		if (input[3]) {
			movement -= new Vector3(1, 0);
		}
		movement = movement.normalized;
		transform.position += movement * cursorSpeed * Time.deltaTime;
	}
}
